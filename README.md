
## Peixe App Readme

En el directorio raiz del proyecto (al mismo nivel de package.json), puede correr los siguientes comandos:

### `npm install`

Instala las dependencias de la aplicación en el directorio /node_modules.


### `npm start`

Inicia la aplicación en modo desarrollo.
Abrir [http://localhost:3000](http://localhost:3000) para ver en el navegador.


### Importante:
El comando `npm install` debe ser el primero en ser ejecutado para que la aplicación funcione correctamente.</br>