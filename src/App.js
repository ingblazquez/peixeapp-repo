import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Layout from "./components/Layout";
import LandingPage from "./components/LandingPage";
import FavoritesPage from "./components/FavoritesPage";

function App() {
 return (
  <Layout>
   <Switch>
    <Route exact path="/home" component={LandingPage} />
    <Route exact path="/favorites" component={FavoritesPage} />
    <Redirect to="/home" />
   </Switch>
  </Layout>
 );
}

export default App;
