import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import actions from "../../redux/dialog/actions";
import "./dialog.css";

const styles = (theme) => ({
 root: {
  margin: 0,
  padding: theme.spacing(2),
 },
 closeButton: {
  position: "absolute",
  right: theme.spacing(1),
  top: theme.spacing(1),
  color: "white",
 },
});

const DialogTitle = withStyles(styles)((props) => {
 const { children, classes, onClose, ...other } = props;
 return (
  <MuiDialogTitle disableTypography className={classes.root} {...other}>
   <Typography variant="h6">{children}</Typography>
   {onClose ? (
    <IconButton
     aria-label="close"
     className={classes.closeButton}
     onClick={onClose}
    >
     <CloseIcon />
    </IconButton>
   ) : null}
  </MuiDialogTitle>
 );
});

const DialogContent = withStyles((theme) => ({
 root: {
  padding: theme.spacing(2),
 },
}))(MuiDialogContent);

export default function CustomizedDialogs() {
 const dispatch = useDispatch();
 const { loading, error, content, open } = useSelector(({ dialog }) => dialog);
 const {
  Title,
  Year,
  Released,
  Director,
  Writer,
  Actors,
  Country,
  Runtime,
  Genre,
  Plot,
 } = content;
 const handleClose = () => {
  dispatch(actions.setOpen(false));
 };

 if (loading)
  return (
   <Backdrop id="loading-backdrop" open>
    <CircularProgress color="inherit" />
   </Backdrop>
  );

 if (error) return console.log(error);

 return (
  <Dialog
   onClose={handleClose}
   aria-labelledby="customized-dialog-title"
   open={open}
  >
   <DialogTitle onClose={handleClose}>
    <div className="dialog-title">
     <span className="dialog-title__title">
      {Title} <span className="dialog-title__title-year">({Year})</span>
     </span>
     <br />
     <span className="dialog-title__description">
      {Genre} | {Runtime} | {Released} | ({Country})
     </span>
    </div>
   </DialogTitle>
   <DialogContent dividers>
    <Typography gutterBottom>{Plot}</Typography>
    <Typography gutterBottom>
     <b>Director: </b>
     {Director}
    </Typography>
    <Typography gutterBottom>
     <b>Writer: </b>
     {Writer}
    </Typography>
    <Typography gutterBottom>
     <b>Stars: </b>
     {Actors}
    </Typography>
   </DialogContent>
  </Dialog>
 );
}
