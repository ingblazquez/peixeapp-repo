import React from "react";
import { useDispatch, useSelector } from "react-redux";
import ErrorIcon from "@material-ui/icons/Error";
import Grid from "@material-ui/core/Grid";
import actions from "../../redux/dialog/actions";
import favActions from "../../redux/favorites/actions";

const FavoritesPage = () => {
 const dispatch = useDispatch();
 const favorites = useSelector(({ favorites }) => favorites.list);
 const handleClick = (e, imdbID) => {
  e.preventDefault();
  dispatch(actions.fetchContent(imdbID));
 };
 const handleFavoriteClick = (e, item) => {
  e.preventDefault();
  dispatch(favActions.toggleFavorite(item));
 };

 if (favorites.length === 0)
  return (
   <div className="home-content">
    <div className="home-content-icon">
     <ErrorIcon />
    </div>
    <div className="home-content-text">
     There are no favorites yet, please add some..
    </div>
   </div>
  );

 return (
  <Grid item xs={12} className="movies">
   {favorites.map(({ Title, Year, Type, Poster, imdbID, favorite }) => (
    <div className="movies-poster">
     <img alt={Title} className="movies-poster__image" src={Poster}></img>
     <div className="movies-poster__overlay-content">
      <b>{Title}</b>
      <small className="displayBlock">
       Type: {Type} Year: {Year}
      </small>
     </div>
     <button
      className="movies-poster__overlay-button"
      onClick={(e) => handleClick(e, imdbID)}
     >
      SEE MORE
     </button>
     <div
      className="movies-poster__favorite-badge"
      onClick={(e) =>
       handleFavoriteClick(e, { Title, Year, Type, Poster, imdbID })
      }
     >
      <div className="movies-poster__favorite-badge__text movies-poster__favorite-badge__yellow">
       Favorite
      </div>
     </div>
    </div>
   ))}
  </Grid>
 );
};

export default FavoritesPage;
