import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import MovieFilterIcon from "@material-ui/icons/MovieFilter";
import ErrorIcon from "@material-ui/icons/Error";
import Skeleton from "@material-ui/lab/Skeleton";
import actions from "../../redux/dialog/actions";
import favActions from "../../redux/favorites/actions";

const LandingPage = () => {
 const dispatch = useDispatch();
 const [controlledMovies, setControlledMovies] = useState([]);
 const { loading, error, movies, notFound } = useSelector(
  ({ moviesList }) => moviesList
 );
 const favorites = useSelector(({ favorites }) => favorites);
 const handleClick = (e, imdbID) => {
  e.preventDefault();
  dispatch(actions.fetchContent(imdbID));
 };
 const handleFavoriteClick = (e, item) => {
  e.preventDefault();
  dispatch(favActions.toggleFavorite(item));
 };

 useEffect(() => {
  setControlledMovies(
   movies.list.map((item) => {
    const favorite = favorites.list.find(
     (element) => element.imdbID === item.imdbID
    );
    return favorite
     ? { ...item, favorite: true }
     : { ...item, favorite: false };
   })
  );
 }, [movies, favorites]);

 if (loading)
  return (
   <div className="skeleton-container">
    <Grid item xs={12}>
     <Grid container justify="center" spacing={6}>
      <Grid key={1} xs={12} md={4} item>
       <Skeleton variant="rect" height={380} />
      </Grid>
      <Grid key={2} xs={12} md={4} item>
       <Skeleton variant="rect" height={380} />
      </Grid>
      <Grid key={3} xs={12} md={4} item>
       <Skeleton variant="rect" height={380} />
      </Grid>
     </Grid>
    </Grid>
   </div>
  );

 if (error) return <p>{error.message}</p>;

 if (notFound)
  return (
   <div className="home-content">
    <div className="home-content-icon">
     <ErrorIcon />
    </div>
    <div className="home-content-text">404 Not found.</div>
   </div>
  );

 if (movies.list.length === 0)
  return (
   <div className="home-content">
    <div className="home-content-icon">
     <MovieFilterIcon />
    </div>
    <div className="home-content-text">
     Online database for movies, television, and video games
    </div>
   </div>
  );

 return (
  <Grid item xs={12} className="movies">
   {controlledMovies.map(({ Title, Year, Type, Poster, imdbID, favorite }) => (
    <div className="movies-poster">
     <img className="movies-poster__image" alt={Title} src={Poster}></img>
     <div className="movies-poster__overlay-content">
      <b>{Title}</b>
      <small className="displayBlock">
       Type: {Type} Year: {Year}
      </small>
     </div>
     <button
      className="movies-poster__overlay-button"
      onClick={(e) => handleClick(e, imdbID)}
     >
      SEE MORE
     </button>
     <div
      className="movies-poster__favorite-badge"
      onClick={(e) =>
       handleFavoriteClick(e, { Title, Year, Type, Poster, imdbID })
      }
     >
      <div
       className={`movies-poster__favorite-badge__text ${
        favorite ? "movies-poster__favorite-badge__yellow" : ""
       }`}
      >
       FAVORITE
      </div>
     </div>
    </div>
   ))}
  </Grid>
 );
};

export default LandingPage;
