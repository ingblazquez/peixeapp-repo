import axios from "axios";
import types from "./types";
const serviceEndpoint = process.env.REACT_APP_OMDBAPI_ENDPOINT;
const omdbapiKey = process.env.REACT_APP_OMDBBAPI_KEY;

const fetchContent = (imdbID) => {
 return async (dispatch) => {
  try {
   dispatch(setLoading(true));
   dispatch(setOpen(true));
   const { data } = await axios.get(
    `${serviceEndpoint}?apikey=${omdbapiKey}&i=${imdbID}`
   );
   //    if (data.Response === "False") return dispatch(setNotFound(true));
   dispatch(setContent(data));
  } catch (err) {
   dispatch(setError(err));
  } finally {
   dispatch(setLoading(false));
  }
 };
};

const setContent = (payload) => ({
 type: types.SET_CONTENT,
 payload,
});

const setOpen = (payload) => ({
 type: types.SET_OPEN,
 payload,
});

const setLoading = (payload) => ({
 type: types.SET_LOADING_CONTENT,
 payload,
});
const setError = (payload) => ({
 type: types.SET_ERROR,
 payload,
});

export default {
 fetchContent,
 setContent,
 setOpen,
 setLoading,
 setError,
};
