import types from "./types";

const INITIAL_STATE = {
 content: {
  Title: "Avengers End Game",
  Year: "2020",
  Released: "04 May 2012",
  Director: "Joss Whedon",
  Writer: "Joss Whedon (screenplay), Zak Penn (story), Joss Whedon (story)",
  Actors: "Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth",
  Country: "USA",
  Runtime: "100 min",
  Genre: "Action",
  Plot:
   "Earth's mightiest heroes must come together and learn to fight as a team if they are going to stop the mischievous Loki and his alien army from enslaving humanity.",
 },
 open: false,
 loading: false,
 error: null,
};

const reducer = (state = INITIAL_STATE, action) => {
 switch (action.type) {
  case types.SET_CONTENT:
   return {
    ...state,
    content: action.payload,
   };
  case types.SET_LOADING_CONTENT:
   return {
    ...state,
    loading: action.payload,
   };
  case types.SET_ERROR:
   return {
    ...state,
    error: action.payload,
   };
  case types.SET_OPEN:
   return {
    ...state,
    open: action.payload,
   };
  default:
   return state;
 }
};

export default reducer;
