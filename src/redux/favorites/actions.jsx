import types from "./types";

const setFavorite = (payload) => ({
 type: types.SET_FAVORITE,
 payload,
});

const removeFavorite = (payload) => ({
 type: types.REMOVE_FAVORITE,
 payload,
});

const toggleFavorite = (payload) => ({
 type: types.TOGGLE_FAVORITE,
 payload,
});

export default {
 setFavorite,
 removeFavorite,
 toggleFavorite,
};
