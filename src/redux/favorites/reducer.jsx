import types from "./types";

const INITIAL_STATE = {
 list: [],
};

const reducer = (state = INITIAL_STATE, action) => {
 switch (action.type) {
  case types.SET_FAVORITE:
   return {
    ...state,
    list: [...state.list, ...[action.payload]],
   };
  case types.REMOVE_FAVORITE:
   return {
    ...state,
    list: state.list.filter((item) => item.imdbID !== action.payload),
   };
  case types.TOGGLE_FAVORITE:
   const favorite = state.list.find(
    (item) => item.imdbID === action.payload.imdbID
   );
   const newFavList = favorite
    ? state.list.filter((item) => item.imdbID !== action.payload.imdbID)
    : [...state.list, ...[action.payload]];
   return {
    ...state,
    list: newFavList,
   };
  default:
   return state;
 }
};

export default reducer;
