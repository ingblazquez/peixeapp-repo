import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import moviesList from "../redux/moviesList/reducer";
import dialog from "../redux/dialog/reducer";
import favorites from "../redux/favorites/reducer";

const persistConfig = {
 key: "root",
 storage,
 whitelist: ["favorites"],
};

const rootReducer = combineReducers({ moviesList, dialog, favorites });

export default persistReducer(persistConfig, rootReducer);
