import { createStore, compose, applyMiddleware } from "redux";
import { persistStore } from "redux-persist";
import thunk from "redux-thunk";
import rootReducer from "./rootReducer";

const middlewares = [thunk];

const composedEnhancers =
 window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
 rootReducer,
 composedEnhancers(applyMiddleware(...middlewares))
);

export const persistor = persistStore(store);

export default { persistor, store };
